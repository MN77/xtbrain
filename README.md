# Xemy (Xtended Memory)

Xemy stores text pages created with a simplified Creole wiki syntax.

These pages are stored in one or more "stores". A store can be e.g. a SQLite3 database, a MariaDB database or a directory with text files.

Access is primarily through a command-line interface. Different editors can be used, several styles and output programs are supported.

## Author
Michael Nitsche <code@mn77.de>

## License
GNU General Public License Version 3

## Project status
This is the second version and a complete rework of the previous version called "Schnipsel".

The programm is well tested and stable in most cases.

