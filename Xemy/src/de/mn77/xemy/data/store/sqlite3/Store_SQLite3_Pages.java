/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy.data.store.sqlite3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.mn77.base.data.convert.ConvertSequence;
import de.mn77.base.data.datetime.MDateTime;
import de.mn77.base.data.form.FormString;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.error.Err_Runtime;
import de.mn77.ext.db.sql.SQLite3;
import de.mn77.ext.db.util.Lib_Sql;
import de.mn77.tagterminal.data.ITEM_META_TYPE;
import de.mn77.tagterminal.data.container.ItemID;
import de.mn77.tagterminal.data.container.ItemMetaData;
import de.mn77.tagterminal.data.container.SearchResult;
import de.mn77.tagterminal.data.container.SearchResultItem;
import de.mn77.tagterminal.store.I_Store_Items;
import de.mn77.tagterminal.store.I_Store_Tags;
import de.mn77.xemy.data.container.PageData;


/**
 * @author Michael Nitsche
 * @created 20.02.2022
 */
public class Store_SQLite3_Pages implements I_Store_Items<PageData> {

	private static final String fields         = "id,created,changed,crypt,trash,hits,title,text,linestyles,textstyles";
	private static final String fields_add     = "created,changed,crypt,trash,hits,title,text,linestyles,textstyles";
	private static final String fields_search  = "id,title,hits";
	private static final String fields_meta    = "id,created,changed,crypt,trash,hits,title,length(text)";
	private static final String db_table_order = " ORDER BY UPPER(title)"; //TODO Case und Umlaute!!!
	private final SQLite3       db;
	private final int           storeID;
	private final I_Store_Tags  sql3tags;


	public Store_SQLite3_Pages( final int storeID, final SQLite3 db, final I_Store_Tags sql3tags ) {
		this.storeID = storeID;
		this.db = db;
		this.sql3tags = sql3tags;
	}


	public ItemID add( final PageData s ) {
		final StringBuilder sb = new StringBuilder();
		sb.append( "INSERT INTO pages (" );
		sb.append( Store_SQLite3_Pages.fields_add );
//		"created,changed,crypt,trash,hits,title,text,linestyles,textstyles";
		sb.append( ") VALUES (" );
//		strftime('%s','2004-01-01 02:34:56')
		sb.append( "strftime('%s','" );
		sb.append( s.getCreated().toString() );
		sb.append( "')," );
		sb.append( "strftime('%s','" );
		sb.append( s.getChanged().toString() );
		sb.append( "')," );
		sb.append( s.crypt );
		sb.append( "," );
		sb.append( s.getTrash() );
		sb.append( "," );
		sb.append( s.getHits() );
		sb.append( "," );

		sb.append( FormString.quoteForSQL( s.getTitle() ) );
		sb.append( "," );
		sb.append( FormString.quoteForSQL( s.text ) );
		sb.append( ",'" );
		sb.append( s.lineStyles );
		sb.append( "','" );
		sb.append( s.textStyles );
		sb.append( "'" );

		sb.append( ")" );

//		MOut.print(sb.toString());

		final int changed = this.db.update( sb.toString() ); // TODO try wrap: org.sqlite.SQLiteException:
		if( changed != 1 )
			throw new Err_Runtime( "Page could not be saved!" );

		final int id = this.db.query_int( "SELECT " + Lib_Sql.fieldLastID( this.db.getDBMS() ) + " FROM pages" ); // vielleicht auch ohne "FROM pages"
//		return db().gibInt("SELECT id FROM schnipsel WHERE text="+text+" AND erstellt='"+jetzt.toString()+"'");
//		MOut.print(id);
		final ItemID pid = new ItemID( this.storeID, id );

		if( s.getTags() != null )
			for( final String tag : s.getTags() )
				this.sql3tags.classify( pid, tag );

		return pid;
	}

	public List<PageData> getAllItems() {
		final ArrayList<PageData> result = new ArrayList<>();
		final ArrayTable<String> tab = this.db.queryTableString( "SELECT " + Store_SQLite3_Pages.fields + " FROM pages" );
		for( final String[] row : tab )
			result.add( this.iConvert( row ) );
		return result;
	}

//	public List<Integer> getAllIDs() {
//		return this.db.queryColumnInteger("SELECT id FROM pages");
//	}

	public PageData getItem( final ItemID id, final boolean count ) {
		if( count )
			this.iClick( id );
		final List<String> row = this.db.queryRow( "SELECT " + Store_SQLite3_Pages.fields + " FROM pages WHERE id=" + id.internalID );
		return this.iConvert( row.toArray( new String[row.size()] ) );
	}

	public boolean isIdValid( final ItemID id ) {
		return !this.db.queryEmpty( "SELECT id FROM pages WHERE id=" + id.internalID );
	}

	public int getSize( final boolean trash ) {
		return this.db.query_int( "SELECT COUNT(id) FROM pages WHERE trash=" + trash );
	}

	public SearchResult search( final String[] search, final boolean or, final boolean onlyTitle ) {
		final SearchResult result = new SearchResult(true);

		String sql = "SELECT " + Store_SQLite3_Pages.fields_search + " FROM pages WHERE trash=FALSE";
		if( search != null && search.length > 0 )
			if( or ) {
				sql += " AND (";

				for( int i = 0; i < search.length; i++ ) {
					String s = search[i];
					if( i > 0 )
						sql += " OR";
					s = FormString.quoteForSQL( '%' + s.toLowerCase() + '%' );
					if( onlyTitle )
						sql += " LOWER(title) LIKE " + s;
					else
						sql += " LOWER(title) LIKE " + s + " OR (crypt=FALSE AND LOWER(text) LIKE " + s + " )";
				}
				sql += " )";
			}
			else
				//			final String[] liste = Lib_Xemy.search(search);
				for( String s : search ) {
					sql += " AND";
					s = FormString.quoteForSQL( '%' + s.toLowerCase() + '%' );
					if( onlyTitle )
						sql += " LOWER(title) LIKE " + s;
					else
						sql += " (LOWER(title) LIKE " + s + " OR (crypt=FALSE AND LOWER(text) LIKE " + s + " ))";
				}
		sql += Store_SQLite3_Pages.db_table_order;

		final ArrayTable<String> tab = this.db.queryTableString( sql );

		for( final String[] row : tab )
			result.add( this.iConvertSearch( row, true ) );

		return result;
	}

	public SearchResult searchAll() {
		final SearchResult result = new SearchResult(false);
		final ArrayTable<String> tab = this.db.queryTableString( "SELECT " + Store_SQLite3_Pages.fields_search + " FROM pages WHERE trash=FALSE" );

		for( final String[] row : tab )
			result.add( this.iConvertSearch( row, false ) );

		return result;
	}

	public List<ItemMetaData> getAllMetaData() {
		final ArrayList<ItemMetaData> result = new ArrayList<>();
		final ArrayTable<String> tab = this.db.queryTableString( "SELECT " + Store_SQLite3_Pages.fields_meta + " FROM pages WHERE trash=FALSE" );

		for( final String[] row : tab )
			result.add( this.iConvertMeta( row ) );

		return result;
	}

	public Collection<ItemMetaData> requestMetaData( final ITEM_META_TYPE sort, final boolean asc, final int requestedAmount ) {
		final ArrayList<ItemMetaData> result = new ArrayList<>();

		final StringBuilder sql = new StringBuilder();
		sql.append( "SELECT " );
		sql.append( Store_SQLite3_Pages.fields_meta );
		sql.append( " FROM pages" );
		sql.append( " WHERE trash=FALSE" );
		sql.append( " ORDER BY " );

		switch( sort ) {
			case CREATED:
				sql.append( "created" );
				break;
			case CHANGED:
				sql.append( "changed" );
				break;
			case SIZE:
				sql.append("length(text)");
				break;
			case HITS:
			default:
				sql.append( "hits" );
				break;
		}

		sql.append( asc ? " ASC" : " DESC" );
		sql.append( " LIMIT " );
		sql.append( requestedAmount );

		final ArrayTable<String> tab = this.db.queryTableString( sql.toString() );

		for( final String[] row : tab )
			result.add( this.iConvertMeta( row ) );

		return result;
	}

	public SearchResult searchTags( final String[] searchTags, final boolean or ) {
		final SearchResult result = new SearchResult(true);

		// Fetch tag IDs
		final ArrayList<Integer> tagIDs = new ArrayList<>();

		for( final String tagName : searchTags ) {
			final String sql = "SELECT id FROM tags WHERE name ='" + tagName + "'";
			final Integer res = this.db.queryInteger( sql );
			if( res != null )
				tagIDs.add( res );
			else if( !or )
				return result; // Here it is not possible to match all tags
		}

		if( tagIDs.size() == 0 ) // No existing tags found
			return result;

		final String in = ConvertSequence.toString( ",", tagIDs );
		String sql = "SELECT p.id,p.title,p.hits FROM pages AS p JOIN tag_page AS tp ON tp.page_id=p.id WHERE p.trash=FALSE AND tp.tag_id IN (" + in + ") GROUP BY p.id"; // or
		if( !or )
			sql += " HAVING count(*) = " + tagIDs.size();

		final ArrayTable<String> tab = this.db.queryTableString( sql );

		for( final String[] row : tab )
			result.add( this.iConvertSearch( row, true ) );

		return result;
	}

	public SearchResult trash() {
		String sql = "SELECT " + Store_SQLite3_Pages.fields_search + " FROM pages WHERE trash=TRUE";
		sql += Store_SQLite3_Pages.db_table_order;
		final ArrayTable<String> dbResult = this.db.queryTableString( sql );

		final SearchResult result = new SearchResult(false);

		for( final String[] row : dbResult )
			result.add( this.iConvertSearch( row, false ) );

		return result;
	}

	public void update( final PageData s ) {
//		this.db.exec("UPDATE schnipsel SET ast=null WHERE ast=" + ast_id);

		final StringBuilder sb = new StringBuilder();
		sb.append( "UPDATE pages " );
//		"created,changed,crypt,trash,hits,title,text,linestyles,textstyles";
		sb.append( "SET " );

		sb.append( "changed=strftime('%s','now')," );
		sb.append( "crypt=" );
		sb.append( s.crypt );
		sb.append( "," );
//		sb.append("trash=");
//		sb.append(s.trash);
//		sb.append(",");
		sb.append( "title=" );
		sb.append( FormString.quoteForSQL( s.getTitle() ) );
		sb.append( "," );
		sb.append( "text=" );
		sb.append( FormString.quoteForSQL( s.text ) );
		sb.append( "," );
		sb.append( "linestyles=" );
		sb.append( FormString.quoteForSQL( s.lineStyles ) );
		sb.append( "," );
		sb.append( "textstyles=" );
		sb.append( FormString.quoteForSQL( s.textStyles ) );

		sb.append( " WHERE id=" );
		sb.append( s.getID().internalID );

//		MOut.print(sb.toString());

		final int changed = this.db.update( sb.toString() );
		if( changed != 1 )
			throw new Err_Runtime( "Page could not be updated!" );
	}

	private void iClick( final ItemID id ) {
		final String sql = "UPDATE pages SET hits=hits+1 WHERE id=" + id.internalID;
		this.db.exec( sql );
	}

	private PageData iConvert( final String[] row ) {
//		private static final String fields = "id,created,changed,crypt,trash,hits,title,text,linestyles,textstyles";
//		public Data_Page(Integer id, String title, String text, String lineStyles, String textStyles, I_DateTime created, I_DateTime changed, int hits, boolean trash, boolean crypt, String[] tags) {
		final ItemID id = new ItemID( this.storeID, Integer.parseInt( row[0] ) );

		return new PageData(
			id,
			row[6],
			row[7],
			row[8],
			row[9],
			new MDateTime( Long.parseLong( row[1] ) * 1000 ),
			new MDateTime( Long.parseLong( row[2] ) * 1000 ),
			Integer.parseInt( row[5] ), // hits
			this.iResultToBool( row[4] ),
			this.iResultToBool( row[3] ),
			Lib_Store_SQLite3.tags( this.db, id ) );
	}

	private boolean iResultToBool( final String s ) {
		if( s.length() == 1 )
			return s.charAt( 0 ) == '1';
		else
			return Boolean.parseBoolean( s );
	}

	private ItemMetaData iConvertMeta( final String[] row ) {
		return new ItemMetaData( // "id,created,changed,crypt,trash,hits,title,text-length"
			new ItemID( this.storeID, Integer.parseInt( row[0] ) ),
			row[6],
			null, // new String[0] TODO HACK,
			Integer.parseInt( row[5] ),
			new MDateTime( Long.parseLong( row[1] ) * 1000 ),
			new MDateTime( Long.parseLong( row[2] ) * 1000 ),
			this.iResultToBool( row[4] ),
			Long.parseLong( row[7] ) // Text-Length
		);
	}

	private SearchResultItem iConvertSearch( final String[] row, boolean addTags ) {
		final ItemID itemID = new ItemID( this.storeID, Integer.parseInt( row[0] ) );
		final String[] tags = addTags
			? Lib_Store_SQLite3.tags( this.db, itemID )
			: null;

		return new SearchResultItem( itemID, row[1], tags );
	}

	public boolean isInTrash( final ItemID id ) {
		final String sqlSearch = "SELECT trash FROM pages WHERE id=" + id.internalID;
		return this.db.query_boolean( sqlSearch );
	}

	public void moveToTrash( final ItemID id ) {
		final String sql = "UPDATE pages SET trash=TRUE WHERE id=" + id.internalID;
		final int changed = this.db.update( sql );

		if( changed != 1 )
			throw new Err_Runtime( "Page could not be deleted!" );
	}

	public void unTrash( final ItemID id ) {
		final String sql = "UPDATE pages SET trash=FALSE WHERE id=" + id.internalID;
		final int changed = this.db.update( sql );

		if( changed != 1 )
			throw new Err_Runtime( "Page could not be deleted!" );
	}

}
