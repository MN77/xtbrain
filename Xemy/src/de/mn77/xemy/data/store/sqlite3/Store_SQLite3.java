/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy.data.store.sqlite3;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.sys.MOut;
import de.mn77.ext.db.login.Login_SQLite3;
import de.mn77.ext.db.sql.SQLite3;
import de.mn77.ext.db.util.Lib_DBInfoTable;
import de.mn77.ext.db.util.Lib_SqlDB;
import de.mn77.tagterminal.data.container.ItemID;
import de.mn77.tagterminal.store.A_Store;
import de.mn77.tagterminal.store.I_Store_Items;
import de.mn77.tagterminal.store.I_Store_Tags;
import de.mn77.xemy.Main_Xemy;


/**
 * @author Michael Nitsche
 * @created 20.02.2022
 */
public class Store_SQLite3 extends A_Store {

	private static final String MEMORY_IDENT = "Memory";
	private I_Store_Items       tablePages;
	private I_Store_Tags        tableTags;
	private SQLite3             db           = null;
	private final boolean       inMemory;


	public Store_SQLite3( final int storeID, final String[] loginData, final boolean inMemory ) {
		super( storeID, loginData );
		this.inMemory = inMemory;
	}


	public boolean canWrite() {
		return true;
	}

	public boolean canTrash() {
		return true;
	}

	public boolean canTags() {
		return true;
	}

	public boolean canHits() {
		return true;
	}

	public String getType() {
		return "SQLite3";
	}

	public String getInfo() {
		return this.inMemory
			? Store_SQLite3.MEMORY_IDENT
			: super.loginData[0];
	}

	@Override
	protected void connect( final String[] login ) throws Exception {
		Login_SQLite3 dbLogin = null;
		String dbPath = null;

		if( this.inMemory ) {
			Err.ifNot( login.length, 0 );
			dbPath = Store_SQLite3.MEMORY_IDENT;
			dbLogin = new Login_SQLite3();
		}
		else {
			Err.ifOutOfBounds( 1, 1, login.length );
			dbPath = login[0];
			dbLogin = new Login_SQLite3( dbPath );
		}

		this.db = new SQLite3();
		this.db.connect( dbLogin, false );

		final boolean exist = Lib_SqlDB.existTable( this.db, "pages" );

		if( !exist ) {
			MOut.print( "Initializing database: " + dbPath, "Please wait ..." );

			try {
				this.iCreate();
			}
			catch( final Exception e ) {
				throw new Exception( "SQLite3-Database could not be created: " + dbPath );
			}
			MOut.print( "SQLite3-Database initialized" );
		}
		else {
			if( !this.iCheckCorrectDB() )
				throw new Exception( "Invalid database for this programm: " + dbPath );

			final Integer newVersion = this.iUpdate();
			if( newVersion != null )
				MOut.print( "SQLite3-Database updated to version " + newVersion + ": " + dbPath );
		}

		this.tableTags = new Store_SQLite3_Tags( this.db );
		this.tablePages = new Store_SQLite3_Pages( this.getStoreID(), this.db, this.tableTags );
	}

	public void close() {
		this.db.close();
		this.db = null;
	}

	public I_Store_Items items() {
		return this.tablePages;
	}

	public I_Store_Tags tags() {
		return this.tableTags;
	}

	public void delete( final ItemID id ) {
		// --- Declassify ---
		final String[] itemTags = this.tableTags.getClasses( id );
		Err.ifNull( itemTags );

		for( final String tag : itemTags )
			this.tableTags.declassify( id, tag );

		// Bad ... USES will not be updated!
//		final String sql1 = "DELETE FROM tag_page WHERE page_id=" + id.internalID;
//		this.db.update( sql1 );

		final String sql2 = "DELETE FROM pages WHERE id=" + id.internalID;
		final int changed = this.db.update( sql2 );

		if( changed != 1 )
			throw new Err_Runtime( "Page could not be deleted!" );
	}

	public void deleteAll() {
		this.db.update( "DELETE FROM tag_page" );
		this.db.update( "DELETE FROM pages" );
		this.db.update( "DELETE FROM tags" );
	}

	private boolean iCheckCorrectDB() {
		return Lib_DBInfoTable.checkIdent( this.db, Main_Xemy.DB_IDENT, Main_Xemy.DB_IDENT_OLD );
	}


	private void iCreate() {
//		"id,created,changed,crypt,trash,clicks,title,text,linestyles,textstyles"

		Lib_DBInfoTable.create( this.db, Main_Xemy.DB_IDENT );
//		(crypt)
//		passhash
//		passdb


		final String sqlPages = "" +
			"CREATE TABLE pages(" +
			"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
			"created INTEGER NOT NULL," + // 8 bytes = long
			"changed INTEGER NOT NULL," + // 8 bytes = long
			"crypt INTEGER NOT NULL," +
			"trash INTEGER NOT NULL," +
			"hits INTEGER NOT NULL," +
			"title VARCHAR(255) NOT NULL," +
			"text TEXT NOT NULL," +
			"linestyles TEXT NOT NULL," +
			"textstyles TEXT NOT NULL" +
			")";
//		MOut.print(sql);
		this.db.exec( sqlPages );
//		this.db.exec("CREATE INDEX i_changed ON pages (changed DESC)");
//		this.db.exec("CREATE INDEX i_trash ON pages (trash)");
//		this.db.exec("CREATE INDEX i_text ON pages (trash,title,crypt,text)");

		final String sqlTags = "" +
			"CREATE TABLE tags(" +
			"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
			"name VARCHAR(32) NOT NULL," +
			"uses INTEGER NOT NULL" +
			")";
//		MOut.print(sql);
		this.db.exec( sqlTags );
		this.db.exec( "CREATE INDEX i_name ON tags (name)" );

		final String sqlTagPage = "" +
			"CREATE TABLE tag_page(" +
			"tag_id INTEGER NOT NULL REFERENCES tags(id) ON DELETE RESTRICT," +
			"page_id INTEGER NOT NULL REFERENCES pages(id) ON DELETE RESTRICT," +
			"UNIQUE(tag_id,page_id)" +
			")";
		this.db.exec( sqlTagPage );
	}

	private Integer iUpdate() {
//		final int oldVersion = this.db.query_int("SELECT value FROM dbinfo WHERE key='version'");
//		if(oldVersion < 2) {
//
//		}
		return null;
	}

}
