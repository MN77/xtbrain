/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy.data.store.sqlite3;

import java.util.Map;

import de.mn77.base.data.struct.I_Iterable;
import de.mn77.base.data.struct.SimpleMap;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.ext.db.sql.SQLite3;
import de.mn77.tagterminal.data.container.ItemID;
import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.store.I_Store_Tags;


/**
 * @author Michael Nitsche
 * @created 27.02.2022
 */
public class Store_SQLite3_Tags implements I_Store_Tags {

	private final SQLite3 db;


	public Store_SQLite3_Tags( final SQLite3 db ) {
		this.db = db;
	}


	public String[] getClasses( final ItemID id ) {
		return Lib_Store_SQLite3.tags( this.db, id );
	}

	public Map<String,Integer> getTagStats() {
		final String sql = "SELECT name,uses FROM tags ORDER BY name"; // WHERE uses > 0
		ArrayTable<Object> table = this.db.queryTableObject( sql);
		Map<String,Integer> map = new SimpleMap<>();

		for(Object[] row : table) {
			String tag = (String)row[0];
			int uses = (Integer)row[1];
			map.put( tag, uses );
		}

		return map;
	}

	public void declassify( final ItemID id, final String tag ) {
		final String tagID = this.iFetchTagID( tag, false );

		String sql = "DELETE FROM tag_page WHERE tag_id=" + tagID + " AND page_id=" + id.internalID;
		this.db.update( sql );
		sql = "UPDATE tags SET uses=uses-1 WHERE id = " + tagID;
		this.db.update( sql );

		// Maybe delete tag
		sql = "SELECT uses FROM tags WHERE id = " + tagID;
		final int uses = this.db.query_int( sql );

		if( uses == 0 ) {
			sql = "DELETE FROM tags WHERE id=" + tagID;
			this.db.update( sql );
		}
	}

	public void classify( final ItemID id, final String tag ) {
		final String tagID = this.iFetchTagID( tag, true );

		final String sqlSelect = "SELECT COUNT(tag_id) FROM tag_page WHERE tag_id=" + tagID + " AND page_id=" + id.internalID;
		final boolean exists = this.db.query_int( sqlSelect ) > 0;

		if( !exists ) {
			String sql = "INSERT INTO tag_page (tag_id,page_id) VALUES (" + tagID + "," + id.internalID + ")";
			this.db.update( sql );
			sql = "UPDATE tags SET uses=uses+1 WHERE id = " + tagID;
			this.db.update( sql );
		}
	}

	private String iFetchTagID( final String tag, final boolean add ) {
		String sql = "SELECT id FROM tags WHERE name = '" + tag + "'";
		String tagID = this.db.queryString( sql );

		// Create tag
		if( tagID == null ) {
			if( !add )
				throw new TagTerminalException( "Unknown tag: " + tag );

			sql = "INSERT INTO tags (name,uses) VALUES ('" + tag + "',0)";
			this.db.update( sql );

			sql = "SELECT id FROM tags WHERE name = '" + tag + "'";
			tagID = this.db.queryString( sql );
		}

		return tagID;
	}


	public I_Iterable<String> getAllTags() {
		final String sql = "SELECT name FROM tags WHERE uses > 0 ORDER BY name";
		return this.db.queryColumn( sql );
	}

}
