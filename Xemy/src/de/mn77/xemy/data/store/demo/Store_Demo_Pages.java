/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy.data.store.demo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.mn77.base.data.datetime.MDateTime;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.tagterminal.data.ITEM_META_TYPE;
import de.mn77.tagterminal.data.container.ItemID;
import de.mn77.tagterminal.data.container.ItemMetaData;
import de.mn77.tagterminal.data.container.SearchResult;
import de.mn77.tagterminal.data.container.SearchResultItem;
import de.mn77.tagterminal.store.I_Store_Items;
import de.mn77.xemy.data.container.PageData;


/**
 * @author Michael Nitsche
 * @created 13.02.2021
 */
public class Store_Demo_Pages implements I_Store_Items<PageData> {

	private final ArrayList<PageData> data = new ArrayList<>();
	private final int                 storeID;


	public Store_Demo_Pages( final int storeID ) {
		this.storeID = storeID;

		final PageData page1 = new PageData( new ItemID( storeID, 1 ), "Dies ist die Nr. 1", "Und dies //**der**// Text dazu!", " ", "", new MDateTime( 2021, 02, 13, 9, 12, 15 ),
			new MDateTime( 2021, 02, 13, 9, 12, 15 ), 5, false, false, null );
		final PageData page2 = new PageData( new ItemID( storeID, 2 ), "Die zweite Seite", "Bla far foo.", " ", "", new MDateTime( 2021, 02, 12, 7, 32, 56 ),
			new MDateTime( 2021, 02, 13, 8, 10, 45 ), 2,
			false, false, null );
		final PageData page3 = new PageData( new ItemID( storeID, 3 ), "Dritter!!!", "Eins, zwei, drei, ...", " ", "", new MDateTime( 2021, 02, 13, 12, 14, 16 ),
			new MDateTime( 2021, 02, 13, 12, 14, 16 ), 1,
			false, false, null );

		this.data.add( page1 );
		this.data.add( page2 );
		this.data.add( page3 );
	}

	public PageData getItem( final ItemID id, final boolean count ) {
		this.iCheckID( id );
		return this.data.get( id.internalID - 1 );
	}

	public SearchResult search( final String[] sa, final boolean or, final boolean onlyTitle ) {
		final SearchResult result = new SearchResult(false);

		for( final PageData s : this.data ) {
			final String text = onlyTitle ? "" : s.text;

			if( this.iSearch( sa, s.getTitle(), text, or ) ) {
				final SearchResultItem item = new SearchResultItem( s.getID(), s.getTitle(), null );
				result.add( item );
			}
		}

		return result;
	}

	public SearchResult searchAll() {
		final SearchResult result = new SearchResult(false);

		for( final PageData s : this.data ) {
			final SearchResultItem item = new SearchResultItem( s.getID(), s.getTitle(), null );
			result.add( item );
		}

		return result;
	}

	public Collection<ItemMetaData> getAllMetaData() {
		final ArrayList<ItemMetaData> result = new ArrayList<>();

		for( final PageData s : this.data ) {
			final ItemMetaData item = new ItemMetaData( s );
			result.add( item );
		}

		return result;
	}

	public Collection<ItemMetaData> requestMetaData( final ITEM_META_TYPE sort, final boolean asc, final int requestedAmount ) {
		return this.getAllMetaData();
	}

	private boolean iSearch( final String[] search, final String title, final String text, final boolean or ) {

		if( or ) {
			for( final String s : search )
				if( title.toLowerCase().contains( s ) || text.toLowerCase().contains( s ) )
					return true;
			return false;
		}
		else {
			for( final String s : search )
				if( !title.toLowerCase().contains( s ) && !text.toLowerCase().contains( s ) )
					return false;
			return true;
		}
	}

	public List<PageData> getAllItems() {
		final List<PageData> result = new ArrayList<>();
		result.addAll( this.data );
		return result;
	}

	public int getSize( final boolean trash ) {
		return this.data.size();
	}

	public ItemID add( final PageData s ) {
		this.data.add( s );
		return new ItemID( this.storeID, this.data.size() );
	}

	public void update( final PageData s ) {
		this.iCheckID( s.getID() );
		MOut.temp( "Save: ID=" + s.getID().getAbsolute() + "  Title=" + s.getTitle() );
		this.data.set( s.getID().internalID - 1, s );
	}

	public SearchResult trash() {
		return new SearchResult(false);
	}

	public void moveToTrash( final ItemID id ) {}

	public void unTrash( final ItemID id ) {}

	public boolean isInTrash( final ItemID id ) {
		return false;
	}

	public void delete( final ItemID id ) {
		this.iCheckID( id );
		this.data.remove( id.internalID - 1 );
	}

	public void deleteAll() {
		this.data.clear();
	}

	public boolean isIdValid( final ItemID id ) {
		return id.internalID >= 1 && id.internalID <= this.data.size();
	}

	private void iCheckID( final ItemID id ) {
		Err.ifOutOfBounds( 1, this.data.size(), id.internalID );
	}

//	public List<Integer> getAllIDs() {
//		final List<Integer> result = new ArrayList<>();
//		for(final Data_Xemy s : this.data)
//			result.add(s.getItemID());
//		return result;
//	}

	public SearchResult searchTags( final String[] searchTags, final boolean or ) {
		return new SearchResult(false);
	}

}
