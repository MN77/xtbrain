/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy.data.store.demo;

import java.util.Map;

import de.mn77.base.data.struct.I_Iterable;
import de.mn77.base.error.Err;
import de.mn77.tagterminal.data.container.ItemID;
import de.mn77.tagterminal.store.I_Store_Tags;


/**
 * @author Michael Nitsche
 * @created 27.02.2022
 */
public class Store_Demo_Tags implements I_Store_Tags {

	public String[] getClasses( final ItemID pageID ) {
		throw Err.invalid();
	}

	public void declassify( final ItemID pageID, final String tag ) {
		Err.invalid();
	}

	public void classify( final ItemID pageID, final String tag ) {
		Err.invalid();
	}

	public I_Iterable<String> getAllTags() {
		throw Err.invalid();
	}

	public Map<String, Integer> getTagStats() {
		throw Err.invalid();
	}

}
