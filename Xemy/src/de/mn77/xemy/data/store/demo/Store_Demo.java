/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy.data.store.demo;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;
import de.mn77.tagterminal.data.container.ItemID;
import de.mn77.tagterminal.store.A_Store;
import de.mn77.tagterminal.store.I_Store_Items;
import de.mn77.tagterminal.store.I_Store_Tags;


/**
 * @author Michael Nitsche
 * @created 13.02.2021
 */
public class Store_Demo extends A_Store {

	private final Store_Demo_Pages tablePages;
	private final Store_Demo_Tags  tableTags;
//	private I_DataStore_Tree tableTree;
//	private I_DataStore_Config tableConfig;
	private boolean connected = false;


	public Store_Demo( final int storeID ) {
		super( storeID, null );
//		this.tableConfig = new DataStore_Demo_Config();
//		this.tableTree = new DataStore_Demo_Tree();
		this.tablePages = new Store_Demo_Pages( storeID );
		this.tableTags = new Store_Demo_Tags();
	}


	public boolean canWrite() {
		return false;
	}

	public boolean canTrash() {
		return false;
	}

	public boolean canTags() {
		return false;
	}

	public boolean canHits() {
		return false;
	}

	public void delete( final ItemID id ) {
		this.tablePages.delete( id );
	}

	public void deleteAll() {
		this.tablePages.deleteAll();
	}

	public String getType() {
		return "Demo";
	}

	public String getInfo() {
		return "";
	}

	@Override
	protected void connect( final String[] login ) throws Exception {
		if( this.connected )
			throw new Err_Runtime( "Already connected!" );
		this.connected = true;
	}

	public void close() {
		this.connected = false;
	}

	public I_Store_Items items() {
		if( !this.connected )
			Err.invalid();
		return this.tablePages;
	}

	public I_Store_Tags tags() {
		if( !this.connected )
			Err.invalid();
		return this.tableTags;
	}

//	public I_DataStore_Tree storeTree() {
//		if(!this.connected)
//			Err.invalid();
//		return this.tableTree;
//	}

//	public I_DataStore_Config storeConfig() {
//		if(!this.connected)
//			Err.invalid();
//		return this.tableConfig;
//	}

//	public A_SqlDB sqlDB() {
//		throw Err.todo();
//	}

}
