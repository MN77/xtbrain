/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy.data.store.wikidir;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.mn77.base.data.datetime.MDateTime;
import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.util.Lib_Random;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.I_Directory;
import de.mn77.base.sys.file.I_File;
import de.mn77.base.sys.file.Lib_TextFile;
import de.mn77.tagterminal.data.ITEM_META_TYPE;
import de.mn77.tagterminal.data.container.ItemID;
import de.mn77.tagterminal.data.container.ItemMetaData;
import de.mn77.tagterminal.data.container.SearchResult;
import de.mn77.tagterminal.data.container.SearchResultItem;
import de.mn77.tagterminal.store.I_Store_Items;
import de.mn77.xemy.convert.in.ConvertWikiToMeta;
import de.mn77.xemy.convert.in.ConvertWikiToObj;
import de.mn77.xemy.convert.out.ConvertObjToWiki;
import de.mn77.xemy.data.container.PageData;


/**
 * @author Michael Nitsche
 * @created 13.02.2021
 *
 *          Not supportet: tags, trash, hits
 */
public class Store_WikiDir_Pages implements I_Store_Items<PageData> {

	private final int   storeID;
	private I_Directory dir = null;


	public Store_WikiDir_Pages( final int storeID, final I_Directory directory ) {
		this.storeID = storeID;
		this.dir = directory;
	}


	public PageData getItem( final ItemID id, final boolean count ) {
		final I_List<I_File> files = this.dir.contentFilesWithSuffix( "txt" );
		Err.ifOutOfBounds( 1, files.size(), id.internalID );
		return this.iConvert( id, files.get( id.internalID ) );
	}

	public SearchResult search( final String[] sa, final boolean or, final boolean onlyTitle ) {
		final SearchResult result = new SearchResult(false);

		final List<PageData> all = this.getAllItems(); // TODO Speed this up!!!

		for( final PageData item : all ) {
			boolean hit = false;

			if( or ) {
				for( final String s : sa )
					if( item.getTitle().toLowerCase().contains( s ) || !onlyTitle && item.text.toLowerCase().contains( s ) ) {
						hit = true;
						break;
					}
			}
			else {
				hit = true;
				for( final String s : sa )
//					if(!item.text.toLowerCase().contains(s) && !item.title.toLowerCase().contains(s)) {
					if( onlyTitle && !item.getTitle().toLowerCase().contains( s ) || !onlyTitle && !item.text.toLowerCase().contains( s ) && !item.getTitle().toLowerCase().contains( s ) ) {
						hit = false;
						break;
					}
			}

			if( hit ) {
				final SearchResultItem srItem = new SearchResultItem( item.getID(), item.getTitle(), null );
				result.add( srItem );
			}
		}

		return result;
	}

	public SearchResult searchAll() { // no trash
		final SearchResult result = new SearchResult(false);
		int id = 1;

		for( final I_File f : this.dir.contentFilesWithSuffix( "txt" ) ) {
			result.add( this.iConvertSearch( id, f ) );
			id++;
		}
		return result;
	}

	public List<PageData> getAllItems() {
		final ArrayList<PageData> result = new ArrayList<>();
		int id = 1;

		for( final I_File f : this.dir.contentFilesWithSuffix( "txt" ) ) {
			result.add( this.iConvert( new ItemID( this.storeID, id ), f ) );
			id++;
		}
		return result;
	}

	public List<ItemMetaData> getAllMetaData() {
		final ArrayList<ItemMetaData> result = new ArrayList<>();
		int id = 1;

		for( final I_File f : this.dir.contentFilesWithSuffix( "txt" ) ) {
			result.add( this.iConvertMeta( new ItemID( this.storeID, id ), f ) );
			id++;
		}
		return result;
	}

	public Collection<ItemMetaData> requestMetaData( final ITEM_META_TYPE sort, final boolean asc, final int requestedAmount ) {
		// Die Daten werden nachher sowieso gefiltert und sortiert. Somit hier nicht zwingend nötig.
		return this.getAllMetaData();
	}

	public SearchResult searchTags( final String[] searchTags, final boolean or ) {
		return null;
	}

//	public List<Integer> getAllIDs() {
//		final ArrayList<Integer> result = new ArrayList<>();
//
//		final int size = this.dir.contentFilesWithSuffix("txt").size();
//		for(int id = 1; id <= size; id++)
//			result.add(id);
//
//		return result;
//	}

	public SearchResult trash() {
//		Trash-IDs are above normal IDS:   *.txt --> len --> +
		return new SearchResult(false);
	}

	public int getSize( final boolean trash ) {
//		return trash
//			? this.dir.contentFilesWithSuffix("trash").size()
		return this.dir.contentFilesWithSuffix( "txt" ).size();
	}

	public ItemID add( final PageData s ) {

		try {
			final String alternateName = this.iAlternateFilename();

			final String fileName = this.iFileName( s.getTitle(), alternateName );
			I_File file = this.dir.fileMay( fileName, "txt" );

			if( file.exists() )
				file = this.dir.fileAbsent( alternateName, "txt" ); // TODO HACK: Now using timespamp ... should add 2,3,4,5,...?

			final String wiki = new ConvertObjToWiki().convert( s );
			Lib_TextFile.set( file.getFile(), wiki );

			return this.iGetID( fileName + ".txt" );
		}
		catch( final Err_FileSys e ) {
			MOut.print( "Error: " + e.getMessage() );
			return null;
		}
	}

	public void update( final PageData s ) {

		try {
			final I_File file = this.iGetFile( s.getID().internalID );

			if( file == null ) {
				MOut.print( "File not found for ID: " + s.getID().getAbsolute() );
				return;
			}

			final String wiki = new ConvertObjToWiki().convert( s );
			Lib_TextFile.set( file.getFile(), wiki );
		}
		catch( final Err_FileSys e ) {
			MOut.print( "Error: " + e.getMessage() );
		}
	}

	public boolean isIdValid( final ItemID id ) {
		final int size = this.dir.contentFilesWithSuffix( "txt" ).size();
		return size > 0 && id.internalID >= 1 && id.internalID <= size;
	}

	private PageData iConvert( final ItemID id, final I_File f ) {

		try {
			final String text = Lib_TextFile.read( f.getFile() );
			return ConvertWikiToObj.convert( id, text, f.getNameWithoutSuffix(), f.getModifiedDateTime(), f.getModifiedDateTime(), 0, false ); // clicks = 0, trash = false
		}
		catch( final Err_FileSys e ) {
			throw Err.exit( e );
		}
	}

	private ItemMetaData iConvertMeta( final ItemID id, final I_File f ) {

		try {
			final String text = Lib_TextFile.read( f.getFile() );
			return ConvertWikiToMeta.convert( id, text, f.getNameWithoutSuffix(), 0, f.getModifiedDateTime(), f.getModifiedDateTime(), false ); // clicks = 0, trash = false
		}
		catch( final Err_FileSys e ) {
			throw Err.exit( e );
		}
	}

	private SearchResultItem iConvertSearch( final int id, final I_File f ) {

		try {
			final String text = Lib_TextFile.read( f.getFile() );
			final ItemID pid = new ItemID( this.storeID, id );
			return ConvertWikiToMeta.convert( pid, text, f.getNameWithoutSuffix() );
		}
		catch( final Err_FileSys e ) {
			throw Err.exit( e );
		}
	}

	private ItemID iGetID( final String filename ) {
		int id = 1;

		for( final I_File f : this.dir.contentFilesWithSuffix( "txt" ) ) {
			if( f.getName().equals( filename ) )
				return new ItemID( this.storeID, id );
			id++;
		}
		return null;
	}

	private I_File iGetFile( final int id ) {
		int curID = 1;

		for( final I_File f : this.dir.contentFilesWithSuffix( "txt" ) ) {
			if( id == curID )
				return f;
			curID++;
		}
		return null;
	}

	private String iFileName( final String title, final String alternate ) {
		if( title == null || title.trim().length() <= 1 )
			return alternate;

		final StringBuilder sb = new StringBuilder( title.length() );
		for( final char c : title.toCharArray() )
			if( c == ' ' )
				sb.append( '_' );
			else if( c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c >= '0' && c <= '9' || c == '_' )
				sb.append( c );
//			else --> ignore
		return sb.toString();
	}


	public void delete( final ItemID id ) {
		final I_File f = this.iGetFile( id.internalID );
		this.iDelete( f );
	}

	public void deleteAll() {
		for( final I_File f : this.dir.contentFilesWithSuffix( "txt" ) )
			this.iDelete( f );
	}

	private void iDelete( final I_File f ) {

		try {
			f.rename( "." + this.iAlternateFilename() + ".deleted" );
		}
		catch( final Err_FileSys e ) {
			Err.show( e );
		}
	}

	private String iAlternateFilename() {
		return new MDateTime().toStringFileSysSlim() + "_" + Lib_Random.getString( 8, true, true, true, null );
	}

	public boolean isInTrash( final ItemID id ) {
		return false;
	}

	public void moveToTrash( final ItemID id ) {
		this.delete( id );
	}

	public void unTrash( final ItemID id ) {
		Err.invalid( "Undelete is not supported for a 'WikiDir' store." );
	}

}
