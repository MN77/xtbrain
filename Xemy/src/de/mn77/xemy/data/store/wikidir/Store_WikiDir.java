/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy.data.store.wikidir;

import de.mn77.base.error.Err;
import de.mn77.base.sys.file.MDir;
import de.mn77.tagterminal.data.container.ItemID;
import de.mn77.tagterminal.store.A_Store;
import de.mn77.tagterminal.store.I_Store_Items;
import de.mn77.tagterminal.store.I_Store_Tags;


/**
 * @author Michael Nitsche
 * @created 13.02.2021
 */
public class Store_WikiDir extends A_Store {

	private Store_WikiDir_Pages tablePages;
	private Store_WikiDir_Tags  tableTags;


	public Store_WikiDir( final int storeID, final String[] loginData ) {
		super( storeID, loginData );
	}


	public boolean canWrite() {
		return true;
	}

	public boolean canTrash() {
		return false;
	}

	public boolean canTags() {
		return false;
	}

	public boolean canHits() {
		return false;
	}

	@Override
	protected void connect( final String[] login ) throws Exception {
		Err.ifOutOfBounds( 1, 1, login.length );
		final MDir directory = new MDir( login[0] );
		this.tablePages = new Store_WikiDir_Pages( this.getStoreID(), directory );
		this.tableTags = new Store_WikiDir_Tags();
	}

	public void close() {}

	public I_Store_Items items() {
		return this.tablePages;
	}

	public I_Store_Tags tags() {
		return this.tableTags;
	}

	public void delete( final ItemID id ) {
		this.tablePages.delete( id );
	}

	public void deleteAll() {
		this.tablePages.deleteAll();
	}

	public String getType() {
		return "WikiDir";
	}

	public String getInfo() {
		return super.loginData[0];
	}

//	public I_DataStore_Tree storeTree() {
//		if(!this.connected)
//			Err.invalid();
//		return this.tableTree;
//	}

//	public I_DataStore_Config storeConfig() {
//		if(!this.connected)
//			Err.invalid();
//		return this.tableConfig;
//	}

//	public A_SqlDB sqlDB() {
//		throw Err.todo();
//	}

}
