/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy.data.store.mariadb;

import de.mn77.ext.db.sql.MariaDB;
import de.mn77.tagterminal.store.I_Store_Items;
import de.mn77.tagterminal.store.I_Store_Tags;
import de.mn77.tagterminal.store.mariadb.A_Store_MariaDB;
import de.mn77.xemy.Main_Xemy;
import de.mn77.xemy.data.container.PageData;


/**
 * @author Michael Nitsche
 * @created 05.09.2022
 */
public class Store_MariaDB extends A_Store_MariaDB<PageData> {

	/**
	 * loginData = host,port,user,pass,db // host,user,pass,db
	 */
	public Store_MariaDB( final int storeID, final String[] loginData ) {
		super( storeID, loginData, Main_Xemy.DB_IDENT, Main_Xemy.DB_IDENT_OLD );
	}


	@Override
	protected String pCreateItemFields() {
		return "" +
			"crypt INTEGER NOT NULL," +
			"text MEDIUMTEXT NOT NULL," + //TEXT=65535 Zeichen, MEDIUMTEXT = 16777215 Zeichen
			"linestyles MEDIUMTEXT NOT NULL," +
			"textstyles MEDIUMTEXT NOT NULL,";
	}

	@Override
	protected String[] pCreateIndexes() {
		return new String[]{ "CREATE INDEX i_title ON items (title)" };
	}

	@Override
	protected I_Store_Items<PageData> pNewItems( final int storeID, final MariaDB db, final I_Store_Tags tableTags ) {
		return new Store_MariaDB_Pages( this.getStoreID(), db, tableTags );
	}

	@Override
	protected Integer pUpdateMinor( final MariaDB db, int version ) {

//		if( version < 2 ) {
//			db.exec( "RENAME TABLE tag_page TO tagmap" );
//			db.exec( "RENAME TABLE pages TO items" );
//			db.exec( "ALTER TABLE tagmap RENAME COLUMN page_id TO item_id" );
//			version = 2;
//		}

//		return version;
		return null;
	}

}
