/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy.data;

import de.mn77.tagterminal.config.I_TT_Config;
import de.mn77.tagterminal.data.A_DataStoreManager;
import de.mn77.tagterminal.data.I_TaskTool;
import de.mn77.xemy.config.XemyConfig;


/**
 * @author Michael Nitsche
 * @created 08.02.2021
 */
public class DataStoreManager extends A_DataStoreManager {

	private final I_TaskTool tool;


	public DataStoreManager( final String configName ) {
		super( configName );
		this.tool = new PageTaskTool( this );
	}


	@Override
	public I_TaskTool tool() {
		return this.tool;
	}

	@Override
	protected I_TT_Config pNewConfig( final String configName ) {
		return new XemyConfig( configName );
	}

}
