/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy.data;

import java.io.File;
import java.util.Collections;
import java.util.HashSet;

import de.mn77.base.data.datetime.MDateTime;
import de.mn77.base.data.filter.FilterString;
import de.mn77.base.data.group.Group2;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.cmd.SysCmd;
import de.mn77.base.sys.file.Lib_TextFile;
import de.mn77.base.sys.file.MDir;
import de.mn77.base.sys.file.MFile;
import de.mn77.tagterminal.data.EDIT_RESULT;
import de.mn77.tagterminal.data.I_TaskTool;
import de.mn77.tagterminal.data.OUTPUT_FORMAT;
import de.mn77.tagterminal.data.container.I_ItemData;
import de.mn77.tagterminal.exception.TagTerminalException;
import de.mn77.tagterminal.util.Lib_External;
import de.mn77.tagterminal.util.U_TaskTool;
import de.mn77.xemy.Main_Xemy;
import de.mn77.xemy.convert.in.ConvertWikiToObj;
import de.mn77.xemy.convert.out.ConvertObjToColor;
import de.mn77.xemy.convert.out.ConvertObjToHtml;
import de.mn77.xemy.convert.out.ConvertObjToWiki;
import de.mn77.xemy.data.container.PageData;


/**
 * @author Michael Nitsche
 * @created 24.09.2022
 */
public class PageTaskTool implements I_TaskTool<PageData> {

	private static final String CMD_RAW = "diff -d"; // -y	 --suppress-common-lines
	private static final String CMD_GUI = "kdiff3";
	private static final String TEMPLATE = "= \n\n";


	private final DataStoreManager dsm;


	public PageTaskTool( final DataStoreManager dsm ) {
		this.dsm = dsm;
	}

	public PageData clone( final PageData s ) {
		final MDateTime now = new MDateTime();
		return new PageData( null, s.getTitle(), s.text, s.lineStyles, s.textStyles, now, now, 0, false, false, s.getTags() );
	}

	public Object getVersion() {
		return Main_Xemy.versionString();
	}

	public void diff( final PageData page1, final PageData page2, final boolean gui ) {
		final String output1 = new ConvertObjToWiki().convert( page1 );
		final String output2 = new ConvertObjToWiki().convert( page2 );

		try {
			final File tmpFile1 = File.createTempFile( Main_Xemy.PREFIX, ".txt" );
			Lib_TextFile.set( tmpFile1, output1 );

			final File tmpFile2 = File.createTempFile( Main_Xemy.PREFIX, ".txt" );
			Lib_TextFile.set( tmpFile2, output2 );

			// TODO Über Config-File konfigurieren?
			if( gui )
				SysCmd.exec( true, true, PageTaskTool.CMD_GUI, tmpFile1.getAbsolutePath(), tmpFile2.getAbsolutePath(), " >/dev/null 2>/dev/null" );
			else
				SysCmd.exec( true, true, PageTaskTool.CMD_RAW, tmpFile1.getAbsolutePath(), tmpFile2.getAbsolutePath() ); // , "|less" -R

			tmpFile1.delete();
			tmpFile2.delete();
		}
		catch( final Exception e ) {
			Err.exit( e );
		}
	}

	public Group2<EDIT_RESULT, PageData> edit( final PageData s, final boolean gui ) {
		final String wiki = new ConvertObjToWiki().convert( s ) + '\n'; // Add trailing linebreak. It's better to edit

		try {
			final File tmpFile = File.createTempFile( Main_Xemy.PREFIX, ".wiki" );
			Lib_TextFile.set( tmpFile, wiki );
			Lib_External.editFile( this.dsm, tmpFile.getAbsolutePath(), gui );
			final String result = Lib_TextFile.read( tmpFile );
			tmpFile.delete();

			if( wiki.equals( result ) )
				return new Group2<>( EDIT_RESULT.SAME, s );
			else {
				final PageData sEdited = ConvertWikiToObj.convert( s.getID(), result, s.getTitle(), s.getCreated(), s.getChanged(), s.getHits(), s.getTrash() );
				final boolean hasContent = result.trim().length() > 0;
				final EDIT_RESULT state = hasContent ? EDIT_RESULT.CHANGED : EDIT_RESULT.DELETED;
				return new Group2<>( state, sEdited );
			}
		}
		catch( final TagTerminalException e ) {
			throw e;
		}
		catch( final Exception e ) {
//			MOut.error(e);
			throw new TagTerminalException( e.getMessage() );
		}
	}

	public PageData combine( final PageData s1, final PageData s2 ) {
		final MDateTime now = new MDateTime();

		// This way prevents a lot of format errors and keeps the second headline
		final ConvertObjToWiki converter = new ConvertObjToWiki();
		final String wiki1 = converter.convert( s1 );
		final String wiki2 = converter.convert( s2 );
		final String title = s1.getTitle() + " | " + s2.getTitle();

		final String wikiNew = wiki1 + '\n' + Lib_String.sequence( '#', 80 ) + '\n' + wiki2;
		final PageData sComb = ConvertWikiToObj.convert( null, wikiNew, title, now, now, 0, false );

		final String[] tags = this.iCombineTags( s1.getTags(), s2.getTags() );
		return new PageData( null, title, sComb.text, sComb.lineStyles, sComb.textStyles, now, now, 0, false, false, tags );
	}

//	private String iConvertTextStyles(final String textStyles, final int text1length, final int add) {
//		if(textStyles.length() == 0)
//			return textStyles;
//
//		final StringBuilder sb = new StringBuilder();
//		final String[] styles = textStyles.split(",");
//		for(final String s : styles) {
//			if(sb.length() > 0)
//				sb.append(',');
//
//			final int len = s.length();
//			final String sIdx = s.substring(0, len - 2);
//			final String sType = s.substring(len - 2);
//			final int idx = Integer.parseInt(sIdx);
//			Lib_Output.print(s + "  " + sIdx + "  " + idx + "  " + sType);
//			sb.append(idx + text1length + add);
//			sb.append(sType);
//		}
//
//		return null;
//	}

	private String[] iCombineTags( final String[] tags1, final String[] tags2 ) {
		final HashSet<String> set = new HashSet<>();
		if( tags1 != null )
			Collections.addAll( set, tags1 );
		if( tags2 != null )
			Collections.addAll( set, tags2 );
		return set.toArray( new String[set.size()] );
	}

	public PageData importItem( final File file ) {

		try {
			String result = Lib_TextFile.read( file );
			result = FilterString.removeNull( result ); // Bugfix: A (char)0 = EOF will abort StringBuilder and lead to invalid SQL Syntax!

			final MDateTime now = new MDateTime();
			final String alternateName = new MFile( file ).getNameWithoutSuffix();
			return ConvertWikiToObj.convert( null, result, alternateName, now, now, 0, false );
		}
		catch( final Err_FileSys e ) {
//			Err.exit(e);
			throw new TagTerminalException( e.getMessage() );
		}
	}

	public PageData newItem( final boolean gui ) {

		try {
			final File tmpFile = File.createTempFile( Main_Xemy.PREFIX, ".wiki" );
			Lib_TextFile.set( tmpFile, PageTaskTool.TEMPLATE );
			Lib_External.editFile( this.dsm, tmpFile.getAbsolutePath(), gui );
			final String result = Lib_TextFile.read( tmpFile );
			tmpFile.delete();

			if( result.trim().length() <= PageTaskTool.TEMPLATE.trim().length() )
				throw new TagTerminalException( "Blank page discarded." );

			final MDateTime now = new MDateTime();
			return ConvertWikiToObj.convert( null, result, now.toString(), now, now, 0, false );
		}
		catch( final TagTerminalException e ) {
			throw e;
		}
		catch( final Exception e ) {
			throw new TagTerminalException( e.getMessage() );
		}
	}

	public void export( final MDir dir, final PageData page ) {
		final String wiki = new ConvertObjToWiki().convert( page ) + '\n'; // Add trailing newline
		U_TaskTool.exportToTxtFile( dir, page, wiki );
	}

	public String convert( final PageData page, final OUTPUT_FORMAT form ) { // TODO search

		switch( form ) {
			case COLOR:
				return new ConvertObjToColor().convert( page );
			case HTML:
				return new ConvertObjToHtml().convert( page );
			case WIKI:
			default:
				return new ConvertObjToWiki().convert( page );
		}
	}

	public String getPrefix() {
		return Main_Xemy.PREFIX;
	}

	public Group2<EDIT_RESULT, I_ItemData> rename( final PageData s, final boolean gui ) {
		return U_TaskTool.rename( s, gui, Main_Xemy.PREFIX, this.dsm );
	}

}
