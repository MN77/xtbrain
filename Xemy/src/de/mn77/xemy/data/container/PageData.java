/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy.data.container;

import de.mn77.base.data.convert.ConvertArray;
import de.mn77.base.data.datetime.I_DateTime;
import de.mn77.base.data.util.Lib_String;
import de.mn77.tagterminal.data.container.I_ItemData;
import de.mn77.tagterminal.data.container.ItemID;
import de.mn77.tagterminal.data.container.ItemMetaData;


/**
 * @author Michael Nitsche
 * @created 31.01.2021
 */
public class PageData extends ItemMetaData implements I_ItemData {

	public final String text;
	public final String textStyles;
	public final String lineStyles;
//	public final Integer node;
	public final boolean crypt;
//	public final I_Date ABLAUF = DBKiste.neuDatum(this, false);


	public PageData( final ItemID id, final String title, final String text, final String lineStyles, final String textStyles, final I_DateTime created, final I_DateTime changed,
		final int hits, final boolean trash, final boolean crypt, final String[] tags ) {
		/* Integer node, */

		super( id, title, tags, hits, created, changed, trash, text.length() );
		this.text = text;
		this.textStyles = textStyles;
		this.lineStyles = lineStyles;
//		this.node = node;
		this.crypt = crypt;
	}


//	public void setItemID(final int newId) {
//		if(this.id != null)
//			Err.invalid("ID is already set: " + newId + " --> " + this.id);
//		this.id = newId;
//	}

	public String toDescribe() {
		final StringBuilder sb = new StringBuilder();
		sb.append( Lib_String.sequence( '-', 70 ) );
		sb.append( '\n' );
		sb.append( "ID: " );
		sb.append( this.getID() );
		sb.append( '\n' );

		if( this.getID() != null ) {
			sb.append( "Store: " );
			sb.append( this.getID().storeID );
			sb.append( '\n' );
		}
		sb.append( "Created: " );
		sb.append( this.getCreated().toString() );
		sb.append( '\n' );
		sb.append( "Changed: " );
		sb.append( this.getChanged().toString() );
		sb.append( '\n' );
		sb.append( "Title: " );
		sb.append( this.getTitle() );
		sb.append( '\n' );
		sb.append( "TextStyles: " );
		sb.append( this.textStyles );
		sb.append( '\n' );
		sb.append( "LineStyles: " );
		sb.append( this.lineStyles );
		sb.append( '\n' );
		sb.append( "Hits: " );
		sb.append( this.getHits() );
		sb.append( '\n' );
		sb.append( "Trash: " );
		sb.append( this.getTrash() );
		sb.append( '\n' );
		sb.append( "Crypt: " );
		sb.append( this.crypt );
		sb.append( '\n' );
		sb.append( "Tags: " );
		if( this.getTags() != null )
			sb.append( ConvertArray.toString( ", ", (Object[])this.getTags() ) );
		sb.append( '\n' );
		sb.append( "Text:" );
		sb.append( '\n' );
		sb.append( this.text );
		sb.append( '\n' );
		sb.append( Lib_String.sequence( '-', 70 ) );

		return sb.toString();
	}

//	@Deprecated
//	public String getFormat() {
//		return this.format;
//	}
//
//	@Deprecated
//	public void setFormat(String f) {
//		this.format = f;
//	}

}
