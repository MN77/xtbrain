/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy;

import de.mn77.base.version.Lib_Version;
import de.mn77.lib.terminal.CSI_COLOR_BG;
import de.mn77.lib.terminal.CSI_COLOR_FG;
import de.mn77.lib.terminal.MTerminal;
import de.mn77.tagterminal.A_TT_CLI;
import de.mn77.tagterminal.TAG_TERMINAL;
import de.mn77.tagterminal.config.I_TT_Config;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.xemy.data.DataStoreManager;
import de.mn77.xemy.error.Err_Xemy;


/**
 * @author Michael Nitsche
 * @created 13.02.2022
 */
public class Main_CLI extends A_TT_CLI {

	private static final String PROMPT_START = "Xemy ";
	private static final String PROMPT_MARK  = TAG_TERMINAL.DEFAULT_PROMPT_MARK;
	private static final String EXIT_MESSAGE = TAG_TERMINAL.DEFAULT_EXIT_MESSAGE;


	public static void main( final String[] args ) {
		// String[]{"-d","-p1","4","-p2","3","-m","2"};//,"-a","/mnt/musik"};

		try {
			Lib_Version.init( Main_Xemy.VERSION, false ); // Init with error. But "Main_Xemy" should be used anyway!

			final Main_CLI cli = new Main_CLI();
			cli.start( I_TT_Config.DEFAULT_CONFIG_FILENAME );
		}
		catch( final Err_Xemy u ) {
			u.show();
		}
		catch( final Throwable t ) {
			new Err_Xemy( "Internal Xemy error!" ).show( t );
		}
	}


	@Override
	protected void pHeadline( final MTerminal terminal ) {
		final String version_cli = Main_Xemy.VERSION.toStringShort();//.toFormat("CLI %1.%2.%3"); // (%b)
		final String version_date = Main_Xemy.VERSION.getDateTime().getDate().toString();
//		final String userName = Lib_String.capitalize(Sys.getUserName(), true) + "'s";

		final String delimiter = "  /  ";

		if( terminal.isInRawMode() ) {
//			terminal.print(CSI_COLOR_BG.BLUE, CSI_COLOR_FG.WHITE, userName, " ", Main_Xemy.NAME_LONG, CSI_COLOR_BG.DEFAULT); // GREEN
			terminal.print( CSI_COLOR_BG.BLUE, CSI_COLOR_FG.WHITE, Main_Xemy.NAME_LONG, CSI_COLOR_BG.DEFAULT ); // GREEN
			terminal.print( "  ", CSI_COLOR_FG.GREEN, version_cli ); // CYAN
			terminal.print( CSI_COLOR_FG.DARKGRAY, delimiter ); // BROWN
			terminal.print( CSI_COLOR_FG.LIGHTMAGENTA, version_date ); // GREEN
			terminal.print( CSI_COLOR_FG.DEFAULT );
			terminal.print( "\r\n\n" );
		}
		else {
//			terminal.print(userName, " ", Main_Xemy.NAME_LONG);
			terminal.print( Main_Xemy.NAME_LONG );
			terminal.print( "  " );
			terminal.print( version_cli );
			terminal.print( delimiter );
			terminal.print( version_date );
			terminal.print( "\n\n" );
		}
	}

	@Override
	protected I_DataStoreManager pConnect( final String config ) {
		final I_DataStoreManager dsm = new DataStoreManager( config );

		try {
			dsm.connect();
		}
		catch( final Exception e ) {
			throw new Err_Xemy( e );
		}

		return dsm;
	}

	@Override
	protected String pExitMessage() {
		return Main_CLI.EXIT_MESSAGE;
	}

	@Override
	protected boolean pIsDebug() {
		return Main_Xemy.DEBUG;
	}

	@Override
	protected Object pPromptEnd() {
		return Main_CLI.PROMPT_MARK;
	}

	@Override
	protected Object pPromptStart() {
		return Main_CLI.PROMPT_START;
	}

	@Override
	protected String pStartCmd() {
		return null;
	}

}
