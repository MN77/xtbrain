/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy.config;

import java.io.File;

import de.mn77.base.data.convert.ConvertArray;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.file.I_Directory;
import de.mn77.base.sys.file.Lib_TextFile;
import de.mn77.miniconf.result.MIniConfObject;
import de.mn77.tagterminal.config.A_TT_Config;
import de.mn77.tagterminal.config.I_TT_Config;
import de.mn77.tagterminal.data.ItemView;
import de.mn77.tagterminal.store.I_Store;
import de.mn77.tagterminal.util.Lib_External;
import de.mn77.xemy.Main_Xemy;
import de.mn77.xemy.data.store.demo.Store_Demo;
import de.mn77.xemy.data.store.mariadb.Store_MariaDB;
import de.mn77.xemy.data.store.sqlite3.Store_SQLite3;
import de.mn77.xemy.data.store.wikidir.Store_WikiDir;


/**
 * @author Michael Nitsche
 * @created 04.12.2021
 */
public class XemyConfig extends A_TT_Config {

	public XemyConfig( final String configName ) {
		super( configName );
	}


	@Override
	protected I_Store pObjectToStore( final int storeID, final MIniConfObject ds ) {

		switch( ds.type.toLowerCase() ) {
			case "demo":
				Err.ifNot( ds.args.length, 0, "Arguments" );
				return new Store_Demo( storeID );
//			case "mysql_old":
//				Err.ifNot(ds.args.length, 4, "Arguments");
//				return new Store_MySqlOld(storeID, ds.args);
			case "mariadb":
				Err.ifOutOfBounds( "Arguments", 4, 5, ds.args.length );
				return new Store_MariaDB( storeID, ds.args );
			case "wikidir":
				Err.ifNot( ds.args.length, 1, "Arguments" );
				return new Store_WikiDir( storeID, ds.args );
			case "sqlite3":
				Err.ifNot( ds.args.length, 1, "Arguments" );
				return new Store_SQLite3( storeID, ds.args, false );
			case "memory":
				Err.ifNot( ds.args.length, 0, "Arguments" );
				return new Store_SQLite3( storeID, ds.args, true );
//			case "firebird":
//				Err.ifNot(ds.args.length, 4, "Arguments");
//				return new Store_Firebird(storeID, ds.args);
		}

		return null;
	}

	@Override
	protected void pWriteDefaultConfig( final I_Directory configDir, final File configFile ) {
		final String defaultDB = configDir.fileMay( I_TT_Config.DEFAULT_STORE_FILENAME ).getPathAbsolute();

		final StringBuilder sb = new StringBuilder();
		sb.append( "[General]\n" );
		sb.append( "store = 1\n" );
//		sb.append("crypt = false\n");
		sb.append( "experimental = false\n" );

//		sb.append("\n");
//		sb.append("[ID]\n");
//		sb.append("width = 4\n");

		sb.append( "\n" );
		sb.append( "[Edit]\n" );

		sb.append( "# Editors: " );
		sb.append( ConvertArray.toString( ", ", Lib_External.EDITORS ) );
		sb.append( "\n" );
		sb.append( "editor = " );
		sb.append( Lib_External.EDITORS[0] );
		sb.append( "\n" );

		sb.append( "# X-Editors: " );
		sb.append( ConvertArray.toString( ", ", Lib_External.XEDITORS ) );
		sb.append( "\n" );
		sb.append( "xeditor = " );
		sb.append( Lib_External.XEDITORS[0] );
		sb.append( "\n" );

		sb.append( "\n" );
		sb.append( "[Show]\n" );

		sb.append( "# Formats: " );
		sb.append( ConvertArray.toString( ", ", ItemView.FORMATS ) );
		sb.append( "\n" );
		sb.append( "format = " );
		sb.append( ItemView.FORMATS[0] );
		sb.append( "\n" );

		sb.append( "# Viewers: " );
		sb.append( ConvertArray.toString( ", ", ItemView.VIEWERS ) );
		sb.append( "\n" );
		sb.append( "viewer = " );
		sb.append( ItemView.VIEWERS[0] );
		sb.append( "\n" );

		sb.append( "\n" );
		sb.append( "[Stores]\n" );

		sb.append( "1 = SQLite3( \"" + defaultDB + "\" )\n" );
		sb.append( "\n" );

		try {
			Lib_TextFile.set( configFile, sb.toString() );
		}
		catch( final Err_FileSys e ) {
			Err.exit( e );
		}
	}


	@Override
	protected String pDefaultShowFormat() {
		return ItemView.FORMATS[0];
	}

	@Override
	protected String pDefaultShowViewer() {
		return ItemView.VIEWERS[0];
	}

	@Override
	public String getConfigDirName() {
		return Main_Xemy.USER_CONFIG_DIR;
	}

}
