/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy;

import de.mn77.base.error.Err;
import de.mn77.base.version.Lib_Version;
import de.mn77.tagterminal.config.I_TT_Config;
import de.mn77.tagterminal.control.TaskControl;
import de.mn77.tagterminal.data.I_DataStoreManager;
import de.mn77.xemy.data.DataStoreManager;
import de.mn77.xemy.error.Err_Xemy;


/**
 * @author Michael Nitsche
 * @created 21.02.2021
 */
public class Main_Cmd {

	public static void main( final String[] args ) {
//		args = new String[] {"search", "ffmpeg", "aac"};	// TEST
//		args = new String[] {"search", "ffmpeg", "x264"};	// TEST
//		args = new String[] {"search", "ffmpeg"};	// TEST
//		args = new String[] {"search", "mit"};	// TEST

//		args = new String[] {"show", "21284"};	// TEST
//		args = new String[] {"show", "21266"};	// TEST
//		args = new String[] {"show", "30018"};	// TEST
//		args = new String[] {"edit", "30018"};	// TEST

		try {
			Lib_Version.init( Main_Xemy.VERSION, false ); // Init with error. But "Main_Xemy" should be used anyway!

			final Main_Cmd cmd = new Main_Cmd();
			cmd.start( I_TT_Config.DEFAULT_CONFIG_FILENAME, args );
		}
		catch( final Err_Xemy u ) {
			u.show();
		}
		catch( final Throwable t ) {
			new Err_Xemy( "Internal Xemy error!" ).show( t );
		}
	}

	public void start( final String config, final String[] args ) throws Err_Xemy {
//		if(args.length == 0)
//			Main_Xemy_Cmd.help();

		final I_DataStoreManager dsm = new DataStoreManager( config );

		try {
			dsm.connect();
		}
		catch( final Exception e ) {
			throw new Err_Xemy( e.getMessage() );
		}

		final TaskControl cc = new TaskControl();

		try {
			cc.exec( dsm, args );
		}
		catch( final RuntimeException re ) {
			Err.show( re );
		}
	}

}
