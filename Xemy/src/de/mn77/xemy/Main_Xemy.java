/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy;

import java.util.Map.Entry;

import de.mn77.base.error.Err;
import de.mn77.base.sys.ArgumentParser;
import de.mn77.base.sys.MOut;
import de.mn77.base.version.Lib_Version;
import de.mn77.base.version.data.VersionData_ABC;
import de.mn77.tagterminal.config.I_TT_Config;
import de.mn77.xemy.error.Err_Xemy;


/**
 * @author Michael Nitsche
 * @created 13.02.2022
 */
public class Main_Xemy {

	public static final boolean DEBUG      = false;
	public static final String  NAME_LONG  = "Xtended Memory";
	public static final String  NAME_SHORT = "Xemy";
	public static final String   DB_IDENT        = "xemy";
	public static final String[] DB_IDENT_OLD    = { "schnipsel", "xem2" };
	public static final String   USER_CONFIG_DIR = "xemy";
	public static final String   PREFIX          = "xemy_";

	public static final VersionData_ABC VERSION = new VersionData_ABC( 0, 36, 0 );


	public static void main( final String[] args ) {
//		MOut.reset();
//		args = new String[] {"-d", "--config=test", "a", "1"};
//		args = new String[] {"--version"};

		try {
			Lib_Version.init( Main_Xemy.VERSION, false );

			final ArgumentParser arguments = new ArgumentParser( true, true, true );
			arguments.parse( args );

			final Main_Xemy main = new Main_Xemy();
			main.start( arguments );
		}
		catch( final Err_Xemy u ) {
			u.show();
		}
		catch( final Throwable t ) {
			new Err_Xemy( "Internal Xemy error!" ).show( t );
		}
	}

	public void start( final ArgumentParser arguments ) throws Exception {
		String config = I_TT_Config.DEFAULT_CONFIG_FILENAME;

		for( final String sw : arguments.getSwitches() )
			switch( sw ) {
				case "d":
				case "debug":
					MOut.setDeveloper( true );
					break;
				case "version":
					MOut.print( Main_Xemy.versionString() );
					return;
				case "license":
					MOut.print( Main_Xemy.licenseString() );
					return;
				case "help":
					final Main_Cmd cmd = new Main_Cmd();
					cmd.start( config, new String[]{ "manual" } ); // Uses default config
					return;
				default:
					Err.invalid( "Unknown switch: -" + sw );
			}

		for( final Entry<String, String> e : arguments.getPropertys().entrySet() )
			switch( e.getKey() ) {
				case "config":
					config = e.getValue();
					break;
				default:
					Err.invalid( "Unknown property: --" + e.getKey() );
			}

		if( arguments.getOthers().length == 0 ) {
			final Main_CLI cli = new Main_CLI();
			cli.start( config );
		}
		else {
			final Main_Cmd cmd = new Main_Cmd();
			cmd.start( config, arguments.getOthers() );
		}
	}

	private static Object licenseString() {
		return "Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>\n"
			+ "\n"
			+ "This file is part of Xemy <https://gitlab.com/MN77/xemy>\n"
			+ "\n"
			+ "Xemy is free software: you can redistribute it and/or modify\n"
			+ "it under the terms of the GNU General Public License as published\n"
			+ "by the Free Software Foundation, either version 3 of the License,\n"
			+ "or (at your option) any later version.\n"
			+ "\n"
			+ "Xemy is distributed in the hope that it will be useful,\n"
			+ "but WITHOUT ANY WARRANTY; without even the implied warranty\n"
			+ "of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n"
			+ "See the GNU General Public License for more details.\n"
			+ "\n"
			+ "You should have received a copy of the GNU General Public License\n"
			+ "along with Xemy. If not, see <http://www.gnu.org/licenses/>.";
	}

	public static String versionString() {
		return Main_Xemy.NAME_SHORT + " " + Main_Xemy.VERSION.toStringFull();
	}

}
