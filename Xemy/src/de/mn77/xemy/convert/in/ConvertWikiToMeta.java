/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy.convert.in;

import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.datetime.I_DateTime;
import de.mn77.base.data.filter.FilterString;
import de.mn77.tagterminal.data.container.ItemID;
import de.mn77.tagterminal.data.container.ItemMetaData;
import de.mn77.tagterminal.data.container.SearchResultItem;


/**
 * @author Michael Nitsche
 * @created 13.02.2022
 */
public class ConvertWikiToMeta {

	public static SearchResultItem convert( final ItemID id, final String wiki, final String alternateTitle ) {
		final String title = ConvertWikiToMeta.iTitle( wiki, alternateTitle );
		return new SearchResultItem( id, title, null ); // TODO HACK null?
	}

	public static ItemMetaData convert( final ItemID id, final String wiki, final String alternateTitle, final int hits, final I_DateTime created, final I_DateTime changed, final boolean trash ) {
		final String title = ConvertWikiToMeta.iTitle( wiki, alternateTitle );
		return new ItemMetaData( id, title, null, hits, created, changed, trash, wiki.length() );	// TODO Hack null?
	}

	private static String iTitle( final String text, final String alternateTitle ) {
		final Iterable<String> lines = ConvertString.toLines( text );
		for( final String line : lines )
			if( line.startsWith( "=" ) )
				return FilterString.trim( line, new char[]{ '=', ' ', '\t' }, true, true );
		return alternateTitle;
	}

}
