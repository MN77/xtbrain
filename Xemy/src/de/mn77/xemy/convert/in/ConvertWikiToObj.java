/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy.convert.in;

import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.datetime.I_DateTime;
import de.mn77.base.data.filter.FilterString;
import de.mn77.base.data.group.Group2;
import de.mn77.base.data.group.Group3;
import de.mn77.base.data.group.Group4;
import de.mn77.base.data.struct.I_List;
import de.mn77.base.error.Err;
import de.mn77.tagterminal.data.container.ItemID;
import de.mn77.xemy.data.container.PageData;


/**
 * @author Michael Nitsche
 * @created 13.02.2022
 */
public class ConvertWikiToObj {

	public static PageData convert( final ItemID pid, final String wiki, final String alternateTitle, final I_DateTime created, final I_DateTime changed, final int clicks, final boolean trash ) {
//		String title = ConvertWikiToObj.iTitle(wiki, alternateTitle);
		final Group4<String, String, String, String> g = ConvertWikiToObj.iSplit( wiki, alternateTitle );
		final boolean crypt = false; // Currently, encryption is not used
		final String[] tags = null; // Maybe soon

		return new PageData( pid, g.o2, g.o1, g.o3, g.o4, created, changed, clicks, trash, crypt, tags );
	}

	/**
	 * @return Raw-Text, Title, lineStyles, textStyles
	 */
	private static Group4<String, String, String, String> iSplit( final String wiki, final String alternateTitle ) {
		final Group3<String, String, String> text_title_linestyles = ConvertWikiToObj.iLineStyles( wiki, alternateTitle );
		final Group2<String, String> text_styles = ConvertWikiToObj.iTextStyles( text_title_linestyles.o1, text_title_linestyles.o3 );

		return new Group4<>( text_styles.o1, text_title_linestyles.o2, text_title_linestyles.o3, text_styles.o2 );
	}

	/**
	 * @return text, title, lineStyles
	 */
	private static Group3<String, String, String> iLineStyles( final String wiki, final String alternateTitle ) {
		String title = null;
		final I_List<String> lines = ConvertString.toLines( wiki );
		final StringBuilder sb = new StringBuilder();
		final StringBuilder styles = new StringBuilder( lines.size() );
		boolean leadingLineBreaks = true; // Will be ignored

		// Remove trailing empty lines
		for( int i = lines.size() - 1; i >= 0; i-- )
			if( lines.get( i ).trim().length() == 0 )
				lines.removeIndex( i );
			else
				break;

		// Walk through the lines
		for( final String line : lines )
			if( line.startsWith( "=" ) ) {

				// Only use the first Headline1 as title and don't put it to the text
				if( title == null && leadingLineBreaks && line.length() >= 2 && line.charAt( 1 ) != '=' ) {
					final String trimmed = ConvertWikiToObj.iTrimHeadline( line );

					if( trimmed.length() > 0 ) {
						title = trimmed;
//						leadingLineBreaks = true; // stay
						continue; // Don't put title headline to text
					}
				}

				leadingLineBreaks = false;
				final Group2<Integer, String> headLine = ConvertWikiToObj.iHeadLine( line );

				if( headLine != null ) {
					styles.append( ("" + headLine.o1).charAt( 0 ) );
					sb.append( headLine.o2 );
				}
				else {
					styles.append( ' ' );
					sb.append( line );
				}
				sb.append( '\n' );
			}
			else if( line.startsWith( "  " ) ) {
				leadingLineBreaks = false;
				styles.append( 'c' );
				sb.append( line.trim() );
				sb.append( '\n' );
			}
			else if( line.startsWith( "----" ) && ConvertWikiToObj.iIsLineStyle( line ) ) {
				leadingLineBreaks = false;
				styles.append( '-' );
				sb.append( '\n' );
			}
			else if( !(leadingLineBreaks && line.length() == 0) ) {
				leadingLineBreaks = false;
				styles.append( ' ' );
				sb.append( line );
				sb.append( '\n' );
			}

		if( title == null )
			title = alternateTitle;

		return new Group3<>( sb.toString(), title, styles.toString() );
	}

	private static boolean iIsLineStyle( String line ) {
		line = line.trim();
		for( final char c : line.toCharArray() )
			if( c != '-' )
				return false;
		return true;
	}

	/**
	 * @return headline-nr, headline // Returns null if this is not a headline!
	 */
	private static Group2<Integer, String> iHeadLine( final String line ) {

		for( int i = 0; i < line.length(); i++ ) {
			if( line.charAt( i ) == '=' )
				continue;

			final int depth = i;
			final String text = ConvertWikiToObj.iTrimHeadline( line.substring( i ) );
			if( depth > 6 || text.length() == 0 )
				return null;

			return new Group2<>( depth, text );
		}

		// only '='
		return null;
	}

	private static String iTrimHeadline( final String s ) {
		return FilterString.trim( s, new char[]{ '=', ' ', '\t' }, true, true );
	}

	/**
	 * @return text, textStyles
	 */
	private static Group2<String, String> iTextStyles( final String wiki, final String lineStyles ) {
		final StringBuilder text = new StringBuilder( wiki.length() );
		final StringBuilder styles = new StringBuilder();
		int pointer = 0;
		int lineIndex = 0;
		boolean lineFormat = lineStyles.length() > 0 && lineStyles.charAt( 0 ) != ' ';

		for( int i = 0; i < wiki.length() - 1; i++ ) {
			final char c = wiki.charAt( i );

			if( c == '\n' ) {
				lineIndex++;
				lineFormat = lineStyles.charAt( lineIndex ) != ' ';
			}

			boolean isStyle = false;

			if( !lineFormat )
				if( c == '*' || c == '/' || c == '-' || c == '_' )
					if( ConvertWikiToObj.iIsTextStyle( c, wiki, i ) ) {
						ConvertWikiToObj.iTextStyle( styles, pointer, ConvertWikiToObj.iMapTextStyle( c ) );
						isStyle = true;
					}

			if( isStyle )
				i++;
			else {
				text.append( c );
				pointer++;
			}
		}

		return new Group2<>( text.toString(), styles.toString() );
	}

	private static char iMapTextStyle( final char c ) {

		switch( c ) {
			case '*':
				return 'b';
			case '/':
				return 'i';
			case '-':
				return 's';
			case '_':
				return 'u';
			default:
				throw Err.invalid( c );
		}
	}

	private static boolean iIsTextStyle( final char c, final String wiki, final int i ) {
		// not the first in row


		// is last char
		// next is not the same
		if( i != 0 && wiki.charAt( i - 1 ) == c || i == wiki.length() - 1 || wiki.charAt( i + 1 ) != c )
			return false;

		// next next is also the same
		if( i < wiki.length() - 2 && wiki.charAt( i + 2 ) == c )
			return false;

		// Special case: "https://"
		if( c == '/' && i != 0 && wiki.charAt( i - 1 ) == ':' )
			return false;

		return true;
	}

	private static void iTextStyle( final StringBuilder styles, final int index, final char type ) {
		if( styles.length() > 0 )
			styles.append( ',' );
		styles.append( index );
		styles.append( type );
	}


//	private static String iTitle(String text, String alternateTitle) {
//		ArrayList<String> lines = ConvString.toLines(text);
//		for(String line : lines)
//			if(line.startsWith("="))
//				return FilterString.trim(line, new char[] {'=',' ','\t'}, true, true);
//		return alternateTitle;
//	}

}
