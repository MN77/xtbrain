/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy.convert._test;

import java.io.File;

import de.mn77.base.data.datetime.MDateTime;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.Lib_TextFile;
import de.mn77.xemy.convert.in.ConvertWikiToObj;
import de.mn77.xemy.convert.out.ConvertObjToWiki;
import de.mn77.xemy.data.container.PageData;


/**
 * @author Michael Nitsche
 * @created 15.02.2022
 */
public class Test_Convert {

	public static void main( final String[] args ) {

		try {
			final File tmpFile = new File( "/tmp/test-new.txt" );
			final String result = Lib_TextFile.read( tmpFile );

			MOut.print( result );

			final PageData sNew = ConvertWikiToObj.convert( null, result, new MDateTime().toString(), new MDateTime(), new MDateTime(), 0, false );
			MOut.print(sNew.toDescribe());

			final String check = new ConvertObjToWiki().convert( sNew );
			MOut.print( check );

			MOut.print( result.equals( check )
				? "--- Check passed ---"
				: "--- FAILURE at index " + Lib_String.diffFirst( result, check ) + " ---" );
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

}
