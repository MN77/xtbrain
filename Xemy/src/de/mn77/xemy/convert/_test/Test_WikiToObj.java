/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy.convert._test;

import java.io.File;

import de.mn77.base.data.datetime.MDateTime;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.Lib_TextFile;
import de.mn77.xemy.convert.in.ConvertWikiToObj;
import de.mn77.xemy.convert.out.ConvertObjToWiki;
import de.mn77.xemy.data.container.PageData;


/**
 * @author Michael Nitsche
 * @created 07.03.2022
 */
public class Test_WikiToObj {

	public static void main( final String[] args ) {

		try {
			Test_WikiToObj.go();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

	private static void go() throws Throwable {
		final File f = new File( "/tmp/testtesttest.txt" );
		final String s = Lib_TextFile.read( f );
		MOut.print( s );
		final PageData ds = ConvertWikiToObj.convert( null, s, "Alternate title", new MDateTime(), new MDateTime(), 123, false );
		MOut.print(ds.toDescribe());
		final String re = new ConvertObjToWiki().convert( ds );
		MOut.print( re );
	}

}
