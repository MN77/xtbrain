/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy.convert.out;

import java.util.ArrayList;
import java.util.List;

import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;
import de.mn77.tagterminal.util.Lib_TTOut;
import de.mn77.xemy.data.container.PageData;


/**
 * @author Michael Nitsche
 * @created 13.02.2022
 */
public class ConvertObjToWiki implements I_OutputConverter {

	public String convert( final PageData page ) {
		Err.ifNull( page );
		final StringBuilder sb = new StringBuilder();
		sb.append( "= " );
		sb.append( page.getTitle() );
		sb.append( "\n" );

		final String text = this.iFormat( page.text, page.textStyles );
		this.iFormatLines( sb, text, page.lineStyles );

		return sb.toString();
	}

	private void iFormatLines( final StringBuilder sb, final String text, final String lineStyles ) {
		final List<String> lines = ConvertString.toLines( text );
		Lib_TTOut.checkLines( lines, lineStyles );

		for( int lineIdx = 0; lineIdx < lines.size(); lineIdx++ ) {
			final String line = lines.get( lineIdx );

			final char style = lineStyles.charAt( lineIdx );

			switch( style ) {
				case ' ':
					break;
				case 'c':
					sb.append( "  " );
					break;
				case '-':
					sb.append( "----" );
					break;
				case '1':
					sb.append( "= " );
					break;
				case '2':
					sb.append( "== " );
					break;
				case '3':
					sb.append( "=== " );
					break;
				case '4':
					sb.append( "==== " );
					break;
				case '5':
					sb.append( "===== " );
					break;
				case '6':
					sb.append( "====== " );
					break;

				default:
					throw Err.invalid( lineStyles, style );
			}

			sb.append( line );
			sb.append( '\n' );
		}

		// Remove last linebreak
		sb.setLength( sb.length() - 1 );
	}

	private String iFormat( final String text, final String textStyles ) {
		final StringBuilder sb = new StringBuilder( text );
		final ArrayList<Group2<Integer, Character>> styles = this.iSplitTextStyles( textStyles );

		for( int i = styles.size() - 1; i >= 0; i-- ) {
			final Group2<Integer, Character> style = styles.get( i );
			String fs = "";

			switch( style.o2 ) {
				case 'b':
					fs = "**";
					break;
				case 'i':
					fs = "//";
					break;
				case 'u':
					fs = "__";
					break;
				case 's':
					fs = "--";
					break;
			}
			sb.insert( style.o1, fs );
		}

		return sb.toString();
	}

	private ArrayList<Group2<Integer, Character>> iSplitTextStyles( final String textStyles ) {
		final ArrayList<Group2<Integer, Character>> result = new ArrayList<>();

		if( textStyles.length() == 0 )
			return result;

		final String[] items = textStyles.split( "," );

		for( final String item : items ) {
			final int len = item.length();
			final char c = item.charAt( len - 1 );
			final int index = Integer.parseInt( item.substring( 0, len - 1 ) );

			final Group2<Integer, Character> g2 = new Group2<>( index, c );
			result.add( g2 );
		}

		return result;
	}

}
