/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of Xemy <https://gitlab.com/MN77/xemy>
 *
 * Xemy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Xemy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xemy. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.xemy.convert.out;

import java.util.ArrayList;
import java.util.List;

import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.atomic.IntList;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;
import de.mn77.lib.ansi.ANSI_CS;
import de.mn77.lib.ansi.ANSI_CS_BG;
import de.mn77.lib.ansi.ANSI_CS_FG;
import de.mn77.lib.ansi.ControlStringBuilder;
import de.mn77.lib.ansi.Lib_AnsiEscape;
import de.mn77.tagterminal.util.Lib_TTOut;
import de.mn77.xemy.data.container.PageData;


/**
 * @author Michael Nitsche
 * @created 05.03.2022
 */
public class ConvertObjToColor implements I_OutputConverter {

	private static final int        SCREEN_WIDTH     = 80;
	private static final ANSI_CS_FG COLOR_BOLD       = ANSI_CS_FG.YELLOW;
	private static final ANSI_CS_FG COLOR_ITALIC     = ANSI_CS_FG.LIGHTGREEN;
	private static final ANSI_CS_FG COLOR_BOLDITALIC = ANSI_CS_FG.TURQUISE;
	private static final ANSI_CS_FG COLOR_CODE       = ANSI_CS_FG.GREEN; // YELLOW = zu ähnlich zu weiß | LIGHTBLUE = links?!?
	private static final ANSI_CS_FG COLOR_LINE       = ANSI_CS_FG.DARKGRAY;
	private static final ANSI_CS_FG COLOR_STROKE     = ANSI_CS_FG.DARKGRAY;
	private static final ANSI_CS_FG COLOR_LINK       = ANSI_CS_FG.BLUE;

//	private final ANSI_CS headline = ANSI_CS_BG.DARKGRAY.and(ANSI_CS_FG.LIGHTRED); // Möglich: Weiß auf rot // DarkGray ist kaum sichtbar
	private final ANSI_CS_FG headline = ANSI_CS_FG.LIGHTRED;


	@Override
	public String convert( final PageData page ) {
		Err.ifNull( page );
		final ControlStringBuilder sb = new ControlStringBuilder();

		sb.append( ANSI_CS_BG.BLUE.and( ANSI_CS_FG.GRAY ) );
		sb.append( " Page: " );
		sb.append( ANSI_CS_BG.BLUE.and( ANSI_CS_FG.YELLOW ) );
		sb.append( page.getID().getAbsolute() );

		if( page.getTrash() ) {
			sb.append( ANSI_CS_BG.BLUE.and( ANSI_CS_FG.RED ) );
			sb.append( "   Trash" );
		}
		else {
			sb.append( ANSI_CS_BG.BLUE.and( ANSI_CS_FG.GRAY ) );
			sb.append( "   Hits: " );
			sb.append( ANSI_CS_BG.BLUE.and( ANSI_CS_FG.YELLOW ) );
			sb.append( page.getHits() );
		}

		sb.append( ANSI_CS_BG.BLUE.and( ANSI_CS_FG.GRAY ) );
		sb.append( "   Tags: " );

		if( page.getTags() != null ) {
			sb.append( ANSI_CS_BG.BLUE.and( ANSI_CS_FG.YELLOW ) );
			sb.append( page.getTags(), ", " );
		}
		sb.append( " " );
		sb.append( ' ', ConvertObjToColor.SCREEN_WIDTH - sb.effectiveLength() );

		sb.append( ANSI_CS.RESET );
		sb.append( '\n' );

		this.headline( sb, page.getTitle(), 1 );
		sb.append( "\n" );

		final String text = this.iFormat( page.text, page.textStyles );
		final String text2 = this.iFormatLinks( text );
		this.iFormatLines( sb, text2, page.lineStyles );

		sb.append( '\n' ); // Add trailing empty line for better visibility
		return sb.toString();
	}

	private String iFormatLinks( final String text ) {
		final ControlStringBuilder sb = new ControlStringBuilder();
		final String loText = text.toLowerCase();

		for( int i = 0; i < loText.length(); i++ )
			if( loText.startsWith( "https://", i ) || loText.startsWith( "http://", i ) || loText.startsWith( "ftp://", i ) )
				i = this.iFormatLink( sb, text, i );
			else
				sb.append( text.charAt( i ) );

		return sb.toString();
	}

	private int iFormatLink( final ControlStringBuilder sb, final String text, final int idx ) {
		sb.append( ConvertObjToColor.COLOR_LINK ); // .and(ANSI_CS.UNDERLINE)

		for( int i = idx; i < text.length(); i++ ) {
			final char c = text.charAt( i );

			if( c == ' ' || c == '\n' || c == '\t' ) {
				sb.append( ANSI_CS.RESET );
				sb.append( c );
				return i;
			}
			else
				sb.append( c );
		}
		sb.append( ANSI_CS.RESET );
		return text.length() - 1;
	}

	private void headline( final ControlStringBuilder sb, final String title, final int hl ) {
		// Remove trailing empty lines!
		while( sb.getLastChar() == '\n' )
			sb.removeLast();
		sb.append( "\n" ); // Deleted line break for prior line
		sb.append( "\n" ); // Empty line before Headlines

		final String indent = hl == 1
			? Lib_String.sequence( ' ', Math.max( 0, ConvertObjToColor.SCREEN_WIDTH / 2 - (title.length() + 2) / 2 ) )
			: Lib_String.sequence( ' ', (hl - 2) * 3 ); // 3 is much clearer
		sb.append( indent );
		sb.append( this.headline );
//		sb.append(" ");
		sb.append( title );
//		sb.append(" ");
		sb.append( ANSI_CS.RESET );
		sb.append( "\n" );
	}

	private void iFormatLines( final ControlStringBuilder sb, final String text, final String lineStyles ) {
		final List<String> lines = ConvertString.toLines( text );
		Lib_TTOut.checkLines( lines, lineStyles );

		boolean priorEmpty = true;

		for( int lineIdx = 0; lineIdx < lines.size(); lineIdx++ ) {
			final String line = lines.get( lineIdx );

			final char style = lineStyles.charAt( lineIdx );
			String esc = null;
			Integer hl = null;

			switch( style ) {
				case ' ':
					break;
				case 'c':
					esc = /* "  " + */ ConvertObjToColor.COLOR_CODE.toString();
					break;
				case '-':
					esc = ConvertObjToColor.COLOR_LINE.toString() + Lib_String.sequence( '–', ConvertObjToColor.SCREEN_WIDTH );
					break;
				case '1':
					hl = 1;
					break;
				case '2':
					hl = 2;
					break;
				case '3':
					hl = 3;
					break;
				case '4':
					hl = 4;
					break;
				case '5':
					hl = 5;
					break;
				case '6':
					hl = 6;
					break;

				default:
					throw Err.invalid( lineStyles, style );
			}

			if( hl != null )
				this.headline( sb, line, hl );
			else if( esc != null || line.length() > 0 || !priorEmpty ) {
				if( esc != null )
					sb.append( esc );

				sb.append( line );

				if( esc != null )
					sb.append( ANSI_CS.RESET );

				sb.append( '\n' );
			}

			priorEmpty = line.trim().length() == 0;
		}

		// Remove last linebreak
//		sb.setLength(sb.length() - 1);
	}

	private String iFormat( final String text, final String textStyles ) {
		final StringBuilder sb = new StringBuilder( text.length() * 2 );
		final ArrayList<Group2<Integer, Character>> styles = this.iSplitTextStyles( textStyles );
		boolean openBold = false;
		boolean openItalic = false;
		boolean openUnderline = false;
		boolean openStroke = false;

		int gap = 0;

		for( final Group2<Integer, Character> style : styles ) {
			sb.append( text.substring( gap, style.o1 ) );
			gap = style.o1;

			String fs = "";

			switch( style.o2 ) {
				case 'b':
					openBold = !openBold;
					fs = this.esc( openBold, openItalic, openUnderline, openStroke );
					break;
				case 'i':
					openItalic = !openItalic;
					fs = this.esc( openBold, openItalic, openUnderline, openStroke );
					break;
				case 'u':
					openUnderline = !openUnderline;
					fs = this.esc( openBold, openItalic, openUnderline, openStroke );
					break;
				case 's':
					openStroke = !openStroke;
					fs = this.esc( openBold, openItalic, openUnderline, openStroke );
					break;
			}
			sb.append( fs );
		}

		sb.append( text.substring( gap ) );
		return sb.toString();
	}

	private String esc( final boolean bold, final boolean italic, final boolean underline, final boolean stroke ) {
		final IntList list = new IntList();
		list.add( 0 ); // Reset

		if( bold && italic )
			list.addAll( ConvertObjToColor.COLOR_BOLDITALIC.get() );
		else {
			if( bold )
				list.addAll( ConvertObjToColor.COLOR_BOLD.get() );
			if( italic )
				list.addAll( ConvertObjToColor.COLOR_ITALIC.get() );
		}

		if( underline )
			list.addAll( ANSI_CS.UNDERLINE.get() ); // 24
		if( stroke )
			list.addAll( ConvertObjToColor.COLOR_STROKE.get() );

		return Lib_AnsiEscape.escape( list.toArray() );
	}

	private ArrayList<Group2<Integer, Character>> iSplitTextStyles( final String textStyles ) {
		final ArrayList<Group2<Integer, Character>> result = new ArrayList<>();

		if( textStyles.length() == 0 )
			return result;

		final String[] items = textStyles.split( "," );

		for( final String item : items ) {
			final int len = item.length();
			final char c = item.charAt( len - 1 );
			final int index = Integer.parseInt( item.substring( 0, len - 1 ) );

			final Group2<Integer, Character> g2 = new Group2<>( index, c );
			result.add( g2 );
		}

		return result;
	}

}
